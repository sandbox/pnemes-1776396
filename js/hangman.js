$(function () {
  function setCookie(c_name,value,exdays){
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + exdays);
  var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
  document.cookie = c_name + "=" + c_value;
}
function getCookie(c_name){
  var i,x,y,ARRcookies = document.cookie.split(";");
  for (i = 0; i < ARRcookies.length; i++){
    x = ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
    y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
    x = x.replace(/^\s+|\s+$/g,"");
    if (x == c_name){
      return unescape(y);
    }
  }
}
function appendEndLinks(){
  var $pathname = window.location.href;
  $("#hm_game_content").append('<hr><a href="' + $pathname + '">' + Drupal.t('Start new game.') + '</a>');
  $("#hm_game_content").append('<br><a href="' + hangman_game_data[7] + '/game/hangman">' + Drupal.t('Back to change level.') + '</a>');
}

//charset.
var $charset = hangman_game_data[2];
var $chars = $charset.split(',');

  $(document).ready(function(){
    var $lepesszam = hangman_game_data[3];
    var $nid = hangman_game_data[0];
    var $title = hangman_game_data[1];
    var $hidden = $title.length;
    var $hidden2 = $title;

    $.each($chars, function(key, value) {
      var $rege = new RegExp(value, "g");
      $hidden2 = $hidden2.replace($rege,"_");
    });
    $("#hangman_namefield").html($hidden2);

    var $cookie = getCookie("hm_victories");
    //decrease after this victory number
    var $decrease = parseInt(hangman_game_data[4]);
    if($cookie != null && $cookie != "" && !isNaN($cookie)){
      $("#hangman_total_win").html($cookie);
      var $mustDecrease = hangman_game_data[6];
      if($mustDecrease == 1){
        if($cookie >= $decrease){
          $lepesszam--;
        }
        if($cookie >= $decrease * 2){
          $lepesszam--;
        }
        if($cookie >= $decrease * 3){
          $lepesszam--;
        }
      }
      $("#hangman_failure_chances").html($lepesszam);
    }
    else {
      $("#hangman_failure_chances").html($lepesszam);
    }
  });
  $(".hangman_letters").click(function(){
    var $betu = $(this).attr("id");
    $(this).remove();
    var $title = hangman_game_data[1];
    $(".hangman_letters").each(function(){
      var $rege = new RegExp($(this).attr("id"), "g");
      $title = $title.replace($rege,"_");
    });
    var $temp = $("#hangman_namefield").html();
    //missed
    if($title == $temp){
      $("#hangman_status_message").html(Drupal.t('Missed !'));
      var $hiba = parseInt($("#hangman_failure_chances").html());
      $hiba--;
      //lose
      if($hiba == 0){
        $(".hangman_letters").remove();
        $("#hangman_failure_chances").html('0');
        $("#hangman_namefield").html(hangman_game_data[1]);
        $("#hangman_status_message").html("<h3>" + Drupal.t('You lost the game !') + "</h3>");
        appendEndLinks();
      }
      else {
        $("#hangman_failure_chances").html($hiba);
      }
      var $szam = 8 - $hiba;
      $("#hangman_image_div").html('<img src="' + hangman_game_data[7] + '/' + hangman_game_data[5] + '/pics/hm' + $szam + '.png" />');
    }
    else {
      // win, end game
      if($title == hangman_game_data[1]){
        $(".hangman_letters").remove();
        $("#hangman_namefield").html($title);
        $("#hangman_status_message").html("<h3>" + Drupal.t('You won !') + "</h3>");
        $("#hm_failures").html('');
        var $dmn = hangman_game_data[7];
        $.get($dmn + '/hangman/result/' + hangman_game_data[0], function(data) {
          var out = data;
          $("#hm_game_content").append(out);
        });
        appendEndLinks();
        var $cookie = parseInt(getCookie("hm_victories"));
        if($cookie != null && $cookie != "" && !isNaN($cookie)){
          $cookie++;
          setCookie("hm_victories",$cookie,1);
        }
        else {
          setCookie("hm_victories",1,1);
        }
      }
      else {
        $("#hangman_namefield").html($title);
        $("#hangman_status_message").html(Drupal.t('Hit !'));
      }
    }
  });
});
