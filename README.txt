/**
 * @file
 * README file for Hangman module
 */

Hangman module for play hangman games in Drupal sites.

===============================================
About
===============================================

Current Maintainer: Peter Nemes <pnemes87@yahoo.com>

This module provides a very simple hangman game.
It works like the well known, old hangman: the player must to
figure out a word (mostly a name of a famous person).
All the word's letters replaced with underscores (except spaces).
He has a given number of chances to find the word's letters.
If he correctly chosen a letter (the word contains it),
the letter will apper in the word, if not, he lose one chance.
If he figures out the word, before he runs out of chances, he win.

When the game starts, it show a picture from a gibbet.
After a wrong choice, it start to draw a person on it.
Every wrong answer draws a part of the person. 
When the picture complete(player run out of chances), 
the hanged man completely appers(thats where the name from), the user lose.

The game has 3 different difficulty levels, for a better game experience.
When player choose a level, the game randomize a question from that level.

The module provides administrative page for better utility.
Works with unicode characters, can handle specific letters.

The module stores previous victory numbers, so user know how much game he wins.
This function cookie based, so anonymous users can play too.
--NOTE: this function is very simple, and doesnt integrates deeper in Drupal.
You can do nothing with this, and works only if user's browser handle cookies.

===============================================
Requirements
===============================================

This modules need the following modules enabled:

 - Content (CCK)
 - Content Copy
 - Filefield
 - Imagefield
 - Optionwidgets
 - Text
--NOTE: the modules above is the sub-modules of Content(CCK) module.

===============================================
Install & Usage
===============================================

1. Install the module as a normal drupal module, see description here: 
http://drupal.org/node/70151
--NOTE: This will create a new content type: hangman.
The module has dependencies for CCK (http://drupal.org/project/cck)
and content copy (sub-module of CCK).

2. Add content to the content type hangman.
A title field is the question, it can mostly a famous person or a building.
Picture field is in connection with the question, for exaple: a portrait.
The body field is for a description about a question.
For exaple: the question is George Washington,
the picture is a portrait about him, and the body field is a short biography.
The difficulty field gives you the ability to decide,
on which level appear the question.

3. Administer Hangman module.
The module has an admiistrative page with a few settings.
There you can change the base character set (the letters of the Alphabet).
You can add your own language specific letters, like é,á,ű etc.
The module compatible with unicode character set for multilingual usage.
You can set the number of chances from 4-8,
to make the game more easier or harder.
It is possible to do an automatic decrement in the number of chances.
This is a 3 step decrement, each step decrease a chances with 1.
When player plays and wins, after a given number of victory (editable),
his chances will decrease. If the decrease step is for example 5,
and the number of chances is 8, after 5 victory the player starts only 7,
after 10 victory starts 6, and after/over 15 victory he starts with 5 chances.

4.Paths.
You can find the game start page on http://yourdomain/game/hangman
The administer page of game is http://yourdomain/admin/settings/hangman
