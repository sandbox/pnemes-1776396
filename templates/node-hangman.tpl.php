<?php
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php $sticky == TRUE? print ' sticky' : ''; ?><?php $status == FALSE? print ' node-unpublished' : ''; ?>">
<?php print $picture ?>
  <div class="content clear-block">
    <?php print $content;
    ?>
  </div>
</div>
