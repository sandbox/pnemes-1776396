<?php
?>
<?php global $base_url; ?>
<div id="hm_game_content">
  <strong><?php print t("Guess who's name is hidden !"); ?></strong>
  <div id="hangman_status_message"></div>
  <a id="hm_node_nyertel" href="#"></a>
  <div id="hangman_image_div">
    <img src="<?php print $base_url . '/' . $path . '/pics/hm0.png'; ?>" />
  </div>
  <div id="hangman_namefield">
    name comes here
  </div>
  <div id="hm_failures">
    <div id="hangman_failure_chances"></div>
    <span><?php print t('chances left.'); ?></span>
  </div>
  <div class="clear"></div>
  <br />
  <div id="hangman_letter_list">
  <?php
  $chars = variable_get('hm_charset', 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z');
  $chars = explode(',', $chars);
  foreach($chars as $char): ?>
    <a class="hangman_letters" id="<?php print $char; ?>" ><?php print $char; ?></a>
  <?php endforeach; ?>
  </div>
  <br />
  <div id="hangman_total_win">0</div>
  <span><?php print t('times you won.'); ?></span>
</div>
